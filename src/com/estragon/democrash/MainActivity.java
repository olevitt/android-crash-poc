package com.estragon.democrash;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					InputStream inputStream = MainActivity.this.getApplicationContext().getAssets().open("liste_francais.txt");
					final ArrayList<String> listeMots = readStream(inputStream);
					runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							Intent intent = new Intent(MainActivity.this,CrashActivity.class);
							intent.putExtra("crash", listeMots);
							startActivity(intent);
						}
					});
				}
				catch (Exception e) {
					Log.e("CrashException", "Erreur",e);
				}
			}
		}).start();
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public ArrayList<String> readStream(InputStream inputStream){
		BufferedReader reader = new BufferedReader( new InputStreamReader(inputStream) );
		String ligne = "" ;
		ArrayList<String> contenu = new ArrayList<String>();
		try {
			while ( ( ligne = reader.readLine() ) != null ) {
				contenu.add(ligne);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contenu ;
	}

}
